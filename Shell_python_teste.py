
#aplicando conf no spark submit 
PYSPARK_PYTHON=/usr/bin/python36 \
spark-submit \
  --deploy-mode client \
  --master yarn \
  --conf spark.dynamicAllocation.enabled=true \
  --conf spark.executor.instances=30 \
  --conf spark.executor.memoryOverhead=1024 \
  --conf spark.executor.memory=9G \
  --conf spark.driver.memoryOverhead=2048 \
  --conf spark.driver.memory=13G \
  --conf spark.executor.cores=1 \
  --conf spark.driver.cores=3 \
  --conf spark.default.parallelism=60 \
  --conf spark.sql.shuffle.partitions=60 \
  --conf spark.serializer=org.apache.spark.serializer.KryoSerializer \
  --conf spark.kryoserializer.buffer.max=2047m \
  "${PROJECT_DIR}/single_execution.py" "${1}" "${2}" "${3}" "${4}"


# Exit project folder after everything starts
popd


# Start job, forward all arguments to script
# 1: transformation path (ex: transformation.ds.dataset_enem_transformation)
# 2: transformation class (ex: DatasetEnemTransformation)
# 3: s3 path (ex: raw/ENEM/RESULTADO_ENEM/my_file.parquet)
# 4: id exec (ex: 2763)

import logging
from pyspark.sql import functions as f
from pyspark.sql.types import IntegerType, StringType, DateType, DoubleType 
from pyspark.sql.window import Window
from pyspark.sql.functions to_timestamp



class TesteRaizen():
    def __init__(self, id_execucao):
        self.set_spark_conf()

def read_dataframe(files):
    """
    Return a spark dataframe containing all data from input files.
    They should all have the same schema for aggregation to work.
    """
    logger.info(f'Reading files and storing in dataframe...')
    full_files = [LOCATION + '/'.join([BUCKET_NAME, f]) for f in files]
    return spark.read.load(full_files)


def window_apply(df, columns_key, sorted_fields, desc=True, get_only_first=False):
    """
        Will apply the method partitionBy from Window class of pyspark in a dataframe.
        After this will order dataframe from a determinaded columns.
        If get_only_first, just the row_number one will return.

        Parameters:
            - columns_key: list or str
            - sorted_fields: str
            - desc: bool
            - get_only_first: bool
    """
    if isinstance(columns_key, str):
        columns_key = [columns_key]
    if not isinstance(sorted_fields, str):
        raise Exception("Unsuported type on field sorted_fields. Expected string")

    col = f.col(sorted_fields)
    _window = Window.partitionBy(columns_key)
    _window = _window.orderBy(col.desc()) if desc else _window.orderBy(col)
    row_number_col = f.row_number().over(_window).alias('row_number')
    df = df.select(*df.columns, row_number_col)

    if get_only_first:
        df = df.where('row_number == 1').drop('row_number')

    return df

def set_schema(self, result_df):
    result_df = result_df.withColumn(
        .select(
                f.col('year_month').cast(DateType()),
                f.col('uf').cast(StringType()),
                f.col('product').cast(StringType()),
                f.col('unit').cast(StringType()),
                f.col('volume').cast(DoubleType()),
                f.col(to_timestamp('created_at'),'MM/dd/yyyy')   
        )
    )



def save_dataframe(df, new_path):
    """
    Aggregate and save dataframe in new path.
    """
    logger.info(f'Saving dataframe to new path {new_path}...')
    df.coalesce(1).write.parquet(new_path)


